CC := gcc
CFLAGS := --std=c11 $(if $(DEBUG),-ggdb -O0,-O2) -Wall -Wextra

csvparse_CFILES := src/csvparse.c
csvparse_OBJS   := $(csvparse_CFILES:.c=.o)
graph_CFILES := src/graph.c
graph_OBJS   := $(graph_CFILES:.c=.o)
file_CFILES := src/file.c
file_OBJS   := $(file_CFILES:.c=.o)

TARGETS := src/csvparse src/graph src/file

all: $(TARGETS)

# Liaison de csvparse
csvparse: $(csvparse_OBJS)
	$(CC) -o $@ $^

# Liaison de graph
graph: $(graph_OBJS)
	$(CC) -o $@ $^

# Liaison de file
file: $(file_OBJS)
	$(CC) -o $@ $^

# Compilation
%.o: %.c
	$(CC) $(CFLAGS) -o $@ -c $<

clean:
	rm -f $(TARGETS) $(csvparse_OBJS) $(graph_OBJS) $(file_OBJS)

#  vim: set ts=4 sw=4 tw=120 noet :
