#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// Types
// --------
struct QueueNode {
    char content;           // Contenu du noeud
    struct QueueNode *prev; // Noeud precedent
    struct QueueNode *next; // Noeud suivant
};

typedef struct {
    struct QueueNode *first; // Pointeur vers le premier noeud
    struct QueueNode *last;  // Pointeur vers le dernier noeud
} Queue;

// Prototypes
// ----------

Queue queueCreate();
bool queueIsEmpty(const Queue *s);
void queuePush(Queue *s, char content);
char queuePop(Queue *s);
void queueDelete(Queue *s);

// Main
// ---------

int main() {
    Queue q = queueCreate();
    queuePush(&q, 'a');
    queuePush(&q, 'd');
    queuePop(&q); // On n'utilise pas la valeur de retour
    queuePush(&q, 'b');
    queueDelete(&q);

    return 0;
}

// Implémentation
// ----------

Queue queueCreate() {
    return (Queue) {NULL, NULL};
}

bool queueIsEmpty(const Queue *s) {
    return s->first == NULL;
}

void queuePush(Queue *s, char content) {
    struct QueueNode* n = (struct QueueNode*) malloc(sizeof(struct QueueNode));
    n->next = n->prev = NULL;
    n->content = content;

    if(s->first == NULL) {
        s->first = s->last = n;
    } else {
        s->last->prev = n;
        n->next = s->last;
        s->last = n;
    }
}

char queuePop(Queue *s) {
    struct QueueNode* oldNode = s->first;
    char rContent = oldNode->content;
    s->first = s->first->prev;

    if(!queueIsEmpty(s))
        s->first->next = NULL;

    free(oldNode);
    return rContent;
}

void queueDelete(Queue *s) {
    while(!queueIsEmpty(s))
        queuePop(s);
}
