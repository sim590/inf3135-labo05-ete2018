#include <stdio.h>

int main() {
    printf("digraph {\n");
    
    for(int i = 1; i <= 48; ++i)
        printf("\t%i\n", i);

    for(int i = 1; i <= 48; ++i)
        for(int j = i; j <= 48; ++j)
            if(j % i == 0)
                printf("\t%i -> %i\n", i, j);

    printf("}\n");
}
