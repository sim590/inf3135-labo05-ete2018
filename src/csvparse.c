#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>

#define PRG "csvparse"
#define BUF_LEN 1024

void print_help() {
    printf("Mode d'emploi: %s nom_fichier_villes\n", PRG);
    printf("\n");
    printf("où nom_fichier_villes est le chemin valide vers un");
    printf("fichier contenant les villes au format csv.\n");
}

/* Dit si la chaîne de caractères possède exactement deux virgules
 */
bool has_two_commas(const char* s) {
    const char* j = s;
    for (int i = 0; i < 2; ++i) {
        j = strchr(j, ',');
        if (!j++) return false;
    }
    return !strchr(j, ',');
}

/* Taille la chaîne de caractère afin d'y retirer toutes les occurences du
 * caractère `c`
 */
char* trim(const char* s, char c) {
    char *rs = malloc(strlen(s)*sizeof(char) + 1);
    int j = 0;
    for (int i = 0; i < strlen(s); ++i) {
        if (s[i] != c) {
            rs[j] = s[i];
            ++j;
        }
    }
    rs[j] = '\0';
    return rs;
}

int main(int argc, char *argv[]) {
    char buf[BUF_LEN];
    unsigned int rank = 0;

    if (argc < 2) {
        fprintf(stderr,"Un nom de fichier est requis.\n");
        print_help();
        return -1;
    }

    FILE* file = fopen(argv[1], "r");
    printf("Rang  Nom              Pays  Population\n");
    printf("----  ---              ----  ----------\n");
    while (fgets(buf, BUF_LEN, file)) {
        /* si la ligne ne contient pas exactement deux virgules, on doit la rejeter. */
        if (!has_two_commas(buf)) continue;

        char* name = trim(strtok(buf, ","), '\n');
        char* country = trim(strtok(NULL, ","), '\n');
        char* population = trim(strtok(NULL, ","), '\n');

        /*
         * %04d: (voir man 3 printf) Insérer 4 0s en préfixe du nombre
         *  %*s: (voir man 3 printf) Insère un nombre d'espace donné
         * %.4s: (voir man 3 printf) Afficher maximum 4 caractères de la chaîne
         *       de caractères.
         */
        printf("%04d  %s%*s  %.4s  %*s%s\n",
                ++rank,
                name,
                (int)(15-strlen(name)), "", // ces deux arguments sont lirés par %*s
                country,
                (int)(10-strlen(population)), "", // ces deux arguments sont lirés par %*s
                population);

        /* Les lignes suivantes sont nécessaires. Pouvez-vous dire pourquoi?
         * Voir comment name, country et population sont créés (déf. de trim).
         */
        free(name); free(country); free(population);
    }
    return 0;
}

/* vim:set et sw=4 ts=4 tw=120: */

